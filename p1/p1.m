% Práctica 1
FS = 44000;  % Frecuencia de muestreo
audio_f = audioread('audio.wav');
who;
plot(audio_f);  % Visualizando la señal
soundsc(audio_f, FS);  % Para escuchar el audio

fid = fopen('audio.wav', 'r');  % Otra forma de cargar archivo de audio
y = fread(fid, inf, 'int16');  % Asignando a variable
fclose(fid);  % Cerrando archivo 
figure;
plot(y);
soundsc(y, FS*2);
specgram(y)  % Visualización del espectográma
segment = y(1:50000);  % Segmentando señal
soundsc(segment, FS*2);
figure;
plot(segment);

