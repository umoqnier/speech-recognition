function [energia] = energiaTiempoCorto(voz, len_trama)
    energia = [];
    for i=1:len_trama:length(voz) - len_trama  
        temp = voz(i:i+len_trama);
        energia(end + 1) = sum(temp.^2);
    end
end