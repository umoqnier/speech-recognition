function z=crucesCero(voz, len_trama)
  z = [];
  for i = 1:len_trama:length(voz) - 2 * len_trama
      ind = i+(2*len_trama) - 2;
      jnd = i+len_trama - 1;
      temp_1 = sign(voz(jnd:ind));
      temp_2 = sign(voz(i:i + len_trama - 1));
      z = [z 0.5*sum(abs(temp_1-temp_2))];
  end
end
