function [magnitud] = magnitudTiempoCorto(voz, len_trama)
    magnitud = [];
    for i=1:len_trama:length(voz) - len_trama  
        temp = voz(i:i+len_trama);
        magnitud(end + 1) = sum(abs(temp));
    end
end
