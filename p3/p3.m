% Practica 3: Detección de inicio y fin de palabra
% Barriga Martinez Diego Alberto
audio_1 = fopen('parca.wav');
audio_2 = fopen('estacionamiento.wav');
audio_3 = fopen('soprano.wav');

vect_1 = fread(audio_1, inf, 'int16');
fclose(audio_1);
vect_2 = fread(audio_2, inf, 'int16');
fclose(audio_2);
vect_3 = fread(audio_3, inf, 'int16');
fclose(audio_3);

len_trama = 128;
fs = 44100;  % Frecuencia de muestredo de la grabacion
disp('Escuchando audios a procesar');
soundsc(vect_1, fs * 2);  % Para escuchar audio original
pause(2.5);
soundsc(vect_2, fs * 2);  % Para escuchar audio original
pause(2.5);
soundsc(vect_3, fs * 2);  % Para escuchar audio original
pause(2.5);

% Procesamiento de parca.wav
energia_1 = energiaTiempoCorto(vect_1, len_trama);
magnitud_1 = magnitudTiempoCorto(vect_1, len_trama);
cruces_1 = crucesCero(vect_1, len_trama);  % Por trama

Msn_1 = magnitud_1(1:10);
Zsn_1 = cruces_1(1:10);

UmbSupEnrg_1 = 0.5 * max(magnitud_1);
UmbInfEnrg_1 = median(Msn_1) + 2 * std(Msn_1);
UmbCruCero_1 = median(Zsn_1) + 2 *std(Zsn_1);
disp('UMBRALES DE <parca.wav>');
to_str = ['Umbral Superior de Energía: ', num2str(UmbSupEnrg_1), ' Umbral inferior de energía: ', num2str(UmbInfEnrg_1), ' Umbral de cruces por cero: ', num2str(UmbCruCero_1)];
disp(to_str);

subplot(3,1,1);
plot(energia_1, 'g');
title('Energia tiempo corto "parca.wav"');
figure;
subplot(3,1,1);
plot(magnitud_1, 'g');
title('Magnitud tiempo corto "parca.wav"');
figure;
subplot(3,1,1);
plot(cruces_1, 'g');
title('Cruces por cero "parca.wav"');


% Procesamiento de estacionamiento
energia_2 = energiaTiempoCorto(vect_2, len_trama);
magnitud_2 = magnitudTiempoCorto(vect_2, len_trama);
cruces_2 = crucesCero(vect_2, len_trama);

Msn_2 = magnitud_2(1:10);
Zsn_2 = cruces_2(1:10);

UmbSupEnrg_2 = 0.5 * max(magnitud_2);
UmbInfEnrg_2 = median(Msn_2) + 2 * std(Msn_2);
UmbCruCero_2 = median(Zsn_2) + 2 *std(Zsn_2);
disp('UMBRALES DE <estacionamiento.wav>');
to_str = ['Umbral Superior de Energía: ', num2str(UmbSupEnrg_2), ' Umbral inferior de energía: ', num2str(UmbInfEnrg_2), ' Umbral de cruces por cero: ', num2str(UmbCruCero_2)];
disp(to_str);

figure(1);
subplot(3,1,2);
plot(energia_2, 'r');
title('Energia tiempo corto "estacionamiento.wav"');
figure(2);
subplot(3,1,2);
plot(magnitud_2, 'r');
title('Margnitud tiempo corto "estacionamiento.wav"');
figure(3);
subplot(3,1,2);
plot(cruces_2, 'r');
title('Cruces por cero "estacionamiento.wav"');


% Procesameinto de soprano.wav
energia_3 = energiaTiempoCorto(vect_3, len_trama);
magnitud_3 = magnitudTiempoCorto(vect_3, len_trama);
cruces_3 = crucesCero(vect_3, len_trama);

Msn_3 = magnitud_3(1:10);
Zsn_3 = cruces_3(1:10);

UmbSupEnrg_3 = 0.5 * max(magnitud_3);
UmbInfEnrg_3 = median(Msn_3) + 2 * std(Msn_3);
UmbCruCero_3 = median(Zsn_3) + 2 *std(Zsn_3);
disp('UMBRALES DE <soprano.wav>');
to_str = ['Umbral Superior de Energía: ', num2str(UmbSupEnrg_3), ' Umbral inferior de energía: ', num2str(UmbInfEnrg_3), ' Umbral de cruces por cero: ', num2str(UmbCruCero_3)];
disp(to_str);

figure(1);
subplot(3,1,3);
plot(energia_3, 'k');
title('Energia tiempo corto "soprano.wav"');    
figure(2);
subplot(3,1,3);
plot(magnitud_3, 'k');
title('Magnitud tiempo corto "soprano.wav"'); 
figure(3);
subplot(3,1,3);
plot(cruces_3, 'k');
title('Cruces por cero "soprano.wav"');