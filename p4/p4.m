% Practica 4
% Barriga Martinez Diego Alberto
N = 128;
offset = 100;
p = 8;
audio = fopen('estacionamiento.wav');
audio_vect = fread(audio, inf, 'int16');
fclose(audio);

numero_tramas = length(audio_vect) / 128;
r = [];
ar = [];

for i=1:N:numero_tramas - N
    r = [r autocorrelacion(p, N, audio_vect(i:i+(N-1)))];
end

% plot(r);
% figure;

for i = 1:p:length(r) - p
    ar = [ar levinson(r(i:i+p), p)];
end

% plot(ar);

r_test = [3 2 2 2 2 2 2 2 2 2];
p=3;
[ar_test, e] = levinson(r_test, p);
display(ar_test, e);