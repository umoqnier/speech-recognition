function r = autocorrelacion(p,  N, trama)
%AUTOCORRELACION Summary of this function goes here
%   Detailed explanation goes here
    temp = [];
    for k = 1:p
        for m = 1:N-k
            temp(end + 1) = trama(m) * trama(m+k);
        end
        r(k) = sum(temp);
    end
end

