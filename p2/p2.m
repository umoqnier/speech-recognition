% Práctica 2: Filtros
%1. Diseñar un filtro FIR 
 n = 48;
 Wn = 0.35;
 b = fir1(n, Wn, 'low');
 
%2. Graficar la respuesta en frecuencia
figure;
freqz(b, 1, 512);
title('2. Respuesta en frecuencia FIR');

%3. Diseñar un filtro IIR Butterworth 
n = 9;  % Orden del filtro
fs = 44100;  % Frecuencia de muestredo de la grabacion
frec_corte = 1000;  % Frecuencia de corte
Wn = frec_corte / (fs / 2);  % Frecuencia de corte normalizada

[b, a] = butter(n, Wn, 'low');

%4. Graficas la respuesta en frecuencia
figure;
freqz(b, a, 128, fs);
title('4. Respuesta en frecuencia IIR Butterworth');

% 5 Obtener gráficas de tipos de ventanas
figure;
hold on;
puntos = 128;
plot(hamming(puntos), '--');
plot(hann(puntos), '--');
plot(blackman(puntos), '--');
plot(kaiser(puntos, 10), '--');
title('5. Tipos de ventanas');
legend('Hamming', 'Hann', 'Blackman', 'Kaiser');
hold off;

%6. Con los filtro previos, filtrar un archivo de voz
audio_file = fopen('ganso.wav', 'r');
audio_vect = fread(audio_file, inf, 'int16');
fclose(audio_file);
soundsc(audio_vect, fs * 2);  % Para escuchar audio original
pause(2.5);
audio_fil = filter(b, a, audio_vect);
soundsc(audio_fil, fs * 2);  % Para escuchar audio filtrado
pause(2.5);

%7. Grafique el espectograma de la señal resultante
figure;
specgram(audio_vect);
title('7.1 Voz original');
figure;
specgram(audio_fil);
title('7.2 Voz filtrada');

% 8. Filtro de preénfasis
a = 0.95;
FS = 44000;  % Frecuencia de muestreo
file = fopen('ganso.wav', 'r');
x1 = fread(file, inf, 'int16');
fclose(file);
x1 = x1(8000:36000);
for i=2:length(x1)
    y1(i-1) = x1(i) - (a*x1(i-1));
end
pause(2.5);
soundsc(y1, FS * 2);
figure;
subplot(221), plot(x1), title('"ganso.wav" sin filtrar');
ylabel('[ V ]');
xlabel('[ muestras ]');
xlim([0 8000]);
subplot(222), plot(y1),title('"ganso.wav" filtrado');
ylabel('[ V ]');
xlabel('[ muestras ]');
subplot(223), specgram(x1);
ylabel('[ kHz ]');
xlabel('[ tramas ]');
subplot(224),specgram(y1);
ylabel('[ kHz ]');
xlabel('[ tramas ]');
title('8. Filtro de Preenfasis');


%EXTRA con frecuencia a 4000
frec_corte = 4000;  % Frecuencia de corte
Wn = frec_corte / (fs / 2);  % Frecuencia de corte normalizada

[b, a] = butter(n, Wn, 'low');

%EXTRA 4. Graficas la respuesta en frecuencia
figure;
freqz(b, a, 128, fs);
title('EXT 4. Filtro Butter fs=4000');

%EXTRA 6. Con los filtro previos, filtrar un archivo de voz
audio_fil = filter(b, a, audio_vect);
pause(2.5);
soundsc(audio_fil, fs * 2);  % Para escuchar el audio filtrado
%EXTRA 7. Grafique el espectograma de la señal resultante
figure;
specgram(audio_fil);
title('EXT 7. Voz filtrada con fs=4000');