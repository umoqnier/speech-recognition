% 1. Aplicar un filtro de preéfasis
a = 0.95;
FS = 44000;  % Frecuencia de muestreo
file = fopen('ganso.wav', 'r');
x1 = fread(file, inf, 'int16');
fclose(file);
display('Escuchando audio ORIGINAL');
soundsc(x1, FS * 2);  % Para escuchar el audio original
pause(2.5);
x1 = x1(8000:16000);
for i=2:length(x1)
    y1(i-1) = x1(i) - (a*x1(i-1));
end
display('Escuchando audio FILTRADO');
soundsc(y1', FS * 2);  % Para escuchar el audio original
figure(1);
subplot(221), plot(x1), title('"ganso.wav" sin filtrar');
ylabel('[ V ]');
xlabel('[ muestras ]');
xlim([0 8000]);
subplot(222), plot(y1),title('"ganso.wav" filtrado');
ylabel('[ V ]');
xlabel('[ muestras ]');
subplot(223), specgram(x1);
ylabel('[ kHz ]');
xlabel('[ tramas ]');
subplot(224),specgram(y1);
ylabel('[ kHz ]');
xlabel('[ tramas ]');

